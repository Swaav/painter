// Add logic to this script to poll server every second for updated pixels.
let clientSequence = 0
const clientUpdates = []

function pollForUpdate(){
    const requestOptions = {
        method: "POST",
        headers: {
            accept: "application/json",
            "content-Type": "application/json"
        },
        body: JSON.stringify({
            clientUpdates,
            clientSequence
        })
    }

    clientUpdates.length=0

    fetch("/updates", requestOptions)
        .then(res => res.json())
        .then(res => {
            res.updates.forEach(update => bitmap.setColor(...update, false));
            clientSequence=res.sequence;
            setTimeout(pollForUpdate, 1000)
        })
}

pollForUpdate()