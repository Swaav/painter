const express = require("express")
const port = 3000
const app = express()

app.use(express.static('public'))
app.use(express.json())

let updates = []
// Fill in your request handlers here

app.post("/updates", (req, res)=>{
    const clientUpdates = req.body.clientUpdates
    const clientSequence = req.body.clientSequence

    updates = [...updates, ...clientUpdates];
    res.send({
        updates: updates.slice(clientSequence),
        sequence: updates.length
    })
})


app.listen(port)